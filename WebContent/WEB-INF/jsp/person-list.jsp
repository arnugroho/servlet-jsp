<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<c:set var="idGender" scope="session" value="${gender }"/>
<c:if test="${gender == 2}">  
	<c:set var="checked" scope="session" value="checked"/>
</c:if>
<style>

</style>
<body>
	<div id="PersonForm">
		<form action="person-list.html" method="post">
			<table>
				<tr>
					<td>First Name</td>
					<td>:</td>
					<td><input type="text" name="firstName" value="${firstName }"></td>
				</tr>
				<tr>
					<td>Last Name</td>
					<td>:</td>
					<td><input type="text" name="lastName" value="${lastName }"></td>
				</tr>
				<tr>
					<td>Birthdate</td>
					<td>:</td>
					<td><input type="date" name="birthdate" value="${birthdate }"></td>
				</tr>
				<tr>
					<td>Gender</td>
					<td>:</td>
					<td><input type="radio" name="gender" value="1" 
						<c:if test="${idGender == 1}">  
							<c:out value="checked" />
						</c:if>>Male
						<br>
						<input type="radio" name="gender" value="2" 
						<c:if test="${idGender == 2}">  
							<c:out value="checked" />
						</c:if>>Female
					</td>
				</tr>
				<tr>
					<td>Religion</td>
					<td>:</td>
					<td>
						<c:set var="idReligion" scope="session" value="${religion }"/>
						<select name="religion">
							<c:forEach items="${religionCollection }" var="religion">
								<option value="${religion.idReligion }"  
									<c:out value="${idReligion == religion.idReligion ? 'selected' : '' }"/>
								>
									<c:out value="${religion.religionName }"/>
								</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td>Address</td>
					<td>:</td>
					<td><input type="text" name="address" value="${address }"></td>
				</tr>
				
			</table>
			<input type="hidden" name="action" value="${action }">
			<input type="hidden" name="idPerson" value="${idPerson }">
			<input type="submit" value="${action }">
		</form>
	</div>
	<div id="PersonList">
		<table border="1">
			<thead>
				<tr>
					<td>Id Person</td>
					<td>First Name</td>
					<td>Last Name</td>
					<td>Birthdate</td>
					<td>Gender</td>
					<td>Religion</td>
					<td>Address</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${personCollection}" var="person">
					<tr>
						<td><c:out value="${person.idPerson}" /></td>
						<td><c:out value="${person.firstName}" /></td>
						<td><c:out value="${person.lastName}" /></td>
						<td><c:out value="${person.birthdate}" /></td>
						<td><c:out value="${person.genderName}" /></td>
						<td><c:out value="${person.religionName}" /></td>
						<td><c:out value="${person.address}" /></td>
						<td><a href="person-list.html?action=edit&idPerson=<c:out value="${person.idPerson}"/>">Edit</a></td><td>
						<td>
							<form action="person-list.html" method="post">
								<input type="submit" name="action" value="delete">
								<input type="hidden" name="idPerson" value="<c:out value="${person.idPerson}" />">
							</form>
							
						</td>
					</tr>

				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>