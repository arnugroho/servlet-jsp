package dao;

import java.util.Collection;

import model.Religion;

public interface ReligionDao {
	public Collection<Religion> listReligion () throws Exception;
	public void insertReligion (Religion religion) throws Exception;
	public void updateReligion (Religion religion) throws Exception;
	public void deleteReligion (int idReligion) throws Exception;

}
