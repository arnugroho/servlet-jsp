package dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import dao.PersonDao;
import model.ListPerson;
import model.Person;
import util.DBConnection;

public class PersonDaoImpl implements PersonDao {
	private DBConnection dbConnection = new DBConnection();
	private Connection connection = null;

	@Override
	public Collection<ListPerson> listPerson() throws Exception {
		// TODO Auto-generated method stub
		this.connection = this.dbConnection.getConnection();
		Collection<ListPerson> personCollection = new ArrayList<ListPerson>();
		String sql = "select * from person p inner join gender g on p.gender = g.id_gender"
				+ " inner join religion r on p.religion = r.id_religion";
		PreparedStatement ps = this.connection.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			ListPerson listPerson = new ListPerson();
			listPerson.setIdPerson(rs.getInt("id_person"));
			listPerson.setFirstName(rs.getString("first_name"));
			listPerson.setLastName(rs.getString("last_name"));
			listPerson.setGender(rs.getInt("gender"));
			listPerson.setGenderName(rs.getString("gender_name"));
			listPerson.setReligion(rs.getInt("religion"));
			listPerson.setReligionName(rs.getString("religion_name"));
			listPerson.setBirthdate(rs.getDate("birthdate"));
			listPerson.setAddress(rs.getString("address"));

			personCollection.add(listPerson);
		}
		return personCollection;
	}

	@Override
	public Person getPerson(int idPerson) throws Exception {
		// TODO Auto-generated method stub
		Person person = new Person();
		this.connection = this.dbConnection.getConnection();
		String sql = "select * from person where id_person = ?";
		PreparedStatement ps = this.connection.prepareStatement(sql);
		ps.setInt(1, idPerson);
		ResultSet rs = ps.executeQuery();

		if (rs.next()) {
			
			person.setIdPerson(rs.getInt("id_person"));
			person.setFirstName(rs.getString("first_name"));
			person.setLastName(rs.getString("last_name"));
			person.setGender(rs.getInt("gender"));
			person.setReligion(rs.getInt("religion"));
			person.setBirthdate(rs.getDate("birthdate"));
			person.setAddress(rs.getString("address"));

		}
		return person;
	}

	@Override
	public void insertPerson(Person person) throws Exception {
		// TODO Auto-generated method stub
		this.connection = this.dbConnection.getConnection();
		String sql = "insert into person (id_person, first_name, last_name, birthdate, gender, religion, address) "
				+ " values (nextval('seq_person'),?,?,?,?,?,?)";
		PreparedStatement ps = this.connection.prepareStatement(sql);
		ps.setString(1, person.getFirstName());
		ps.setString(2, person.getLastName());
		ps.setDate(3, new java.sql.Date(person.getBirthdate().getTime()));
		ps.setInt(4, person.getGender());
		ps.setInt(5, person.getReligion());
		ps.setString(6, person.getAddress());
		ps.execute();

	}

	@Override
	public void updatePerson(Person person) throws Exception {
		// TODO Auto-generated method stub
		this.connection = this.dbConnection.getConnection();
		String sql = "update person set first_name = ?, last_name = ?,"
				+ " birthdate = ?, gender = ?, religion = ?, address = ? where id_person = ?";
		PreparedStatement ps = this.connection.prepareStatement(sql);
		ps.setString(1, person.getFirstName());
		ps.setString(2, person.getLastName());
		ps.setDate(3, new java.sql.Date(person.getBirthdate().getTime()));
		ps.setInt(4, person.getGender());
		ps.setInt(5, person.getReligion());
		ps.setString(6, person.getAddress());
		ps.setInt(7, person.getIdPerson());
		ps.execute();

	}

	@Override
	public void deletePerson(int idPerson) throws Exception {
		// TODO Auto-generated method stub
		this.connection = this.dbConnection.getConnection();
		String sql = "delete from person where id_person = ?";
		PreparedStatement ps = this.connection.prepareStatement(sql);
		ps.setInt(1, idPerson);
		ps.execute();

	}

}
