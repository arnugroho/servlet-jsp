package dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import dao.ReligionDao;
import model.Religion;
import util.DBConnection;

public class ReligionDaoImpl implements ReligionDao{
	private DBConnection dbConnection = new DBConnection();
	private Connection connection = null;

	@Override
	public Collection<Religion> listReligion() throws Exception {
		// TODO Auto-generated method stub
		this.connection = this.dbConnection.getConnection();
		Collection<Religion> listReligion = new ArrayList<Religion>();
		String sql = "select * from religion order by id_religion asc";
		PreparedStatement ps = this.connection.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		
		while(rs.next()){
			Religion religion = new Religion();
			religion.setIdReligion(rs.getInt("id_religion"));
			religion.setReligionName(rs.getString("religion_name"));
			listReligion.add(religion);
		}
		return listReligion;
	}

	@Override
	public void insertReligion(Religion religion) throws Exception {
		// TODO Auto-generated method stub
		this.connection = this.dbConnection.getConnection();
		String sql = "insert into religion values (?,?)";
		PreparedStatement ps = this.connection.prepareStatement(sql);
		ps.setInt(1, religion.getIdReligion());
		ps.setString(2, religion.getReligionName());
		ps.execute();
		
		
	}

	@Override
	public void updateReligion(Religion religion) throws Exception {
		// TODO Auto-generated method stub
		this.connection = this.dbConnection.getConnection();
		String sql = "update religion set religion_name = ? where id_religion = ?";
		PreparedStatement ps = this.connection.prepareStatement(sql);
		ps.setString(1, religion.getReligionName());
		ps.setInt(2, religion.getIdReligion());
		ps.execute();
		
	}

	@Override
	public void deleteReligion(int idReligion) throws Exception {
		// TODO Auto-generated method stub
		this.connection = this.dbConnection.getConnection();
		String sql = "delete from religion where id_religion = ?";
		PreparedStatement ps = this.connection.prepareStatement(sql);
		ps.setInt(1, idReligion);
		ps.execute();
		
	}

}
