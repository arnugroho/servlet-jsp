package dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import dao.GenderDao;
import model.Gender;
import util.DBConnection;

public class GenderDaoImpl implements GenderDao {
	private DBConnection dbConnection = new DBConnection();
	private Connection connection = null;

	@Override
	public Collection<Gender> listGender() throws Exception {
		// TODO Auto-generated method stub
		this.connection = this.dbConnection.getConnection();
		Collection<Gender> listGender = new ArrayList<Gender>();
		String sql = "select * from gender";
		PreparedStatement ps = this.connection.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			Gender gender = new Gender();
			gender.setIdGender(rs.getInt("id_gender"));
			gender.setGenderName(rs.getString("gender_name"));
			listGender.add(gender);
		}
		return listGender;
	}

	@Override
	public void insertGender(Gender gender) throws Exception {
		// TODO Auto-generated method stub
		this.connection = this.dbConnection.getConnection();
		String sql = "insert into gender values (?,?)";
		PreparedStatement ps = this.connection.prepareStatement(sql);
		ps.setInt(1, gender.getIdGender());
		ps.setString(2, gender.getGenderName());
		ps.execute();

	}

	@Override
	public void updateGender(Gender gender) throws Exception {
		// TODO Auto-generated method stub
		this.connection = this.dbConnection.getConnection();
		String sql = "update gender set gender_name = ? where id_gender = ?";
		PreparedStatement ps = this.connection.prepareStatement(sql);
		ps.setString(1, gender.getGenderName());
		ps.setInt(2, gender.getIdGender());
		ps.execute();

	}

	@Override
	public void deleteGender(int idGender) throws Exception {
		// TODO Auto-generated method stub
		this.connection = this.dbConnection.getConnection();
		String sql = "delete from gender where id_gender = ?";
		PreparedStatement ps = this.connection.prepareStatement(sql);
		ps.setInt(1, idGender);
		ps.execute();

	}

}
