package dao;

import java.util.Collection;

import model.Gender;


public interface GenderDao {
	public Collection<Gender> listGender () throws Exception;
	public void insertGender (Gender gender) throws Exception;
	public void updateGender (Gender gender) throws Exception;
	public void deleteGender (int idGender) throws Exception;

}
