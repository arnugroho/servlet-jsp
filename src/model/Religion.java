package model;

public class Religion {
	private int idReligion;
	private String religionName;
	
	public int getIdReligion() {
		return idReligion;
	}
	public void setIdReligion(int idReligion) {
		this.idReligion = idReligion;
	}
	public String getReligionName() {
		return religionName;
	}
	public void setReligionName(String religionName) {
		this.religionName = religionName;
	}


}
