package service.impl;

import java.util.Collection;

import dao.PersonDao;
import dao.impl.PersonDaoImpl;
import model.ListPerson;
import model.Person;
import service.PersonService;

public class PersonServiceImpl implements PersonService{
	PersonDao personDao = new PersonDaoImpl();

	@Override
	public Collection<ListPerson> listPerson() throws Exception {
		// TODO Auto-generated method stub
		return personDao.listPerson();
	}

	@Override
	public Person getPerson(int idPerson) throws Exception {
		// TODO Auto-generated method stub
		return personDao.getPerson(idPerson);
	}

	@Override
	public void insertPerson(Person person) throws Exception {
		// TODO Auto-generated method stub
		personDao.insertPerson(person);
		
	}

	@Override
	public void updatePerson(Person person) throws Exception {
		// TODO Auto-generated method stub
		personDao.updatePerson(person);
		
	}

	@Override
	public void deletePerson(int idPerson) throws Exception {
		// TODO Auto-generated method stub
		personDao.deletePerson(idPerson);
		
	}

}
