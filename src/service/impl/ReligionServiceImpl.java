package service.impl;

import java.util.Collection;

import dao.ReligionDao;
import dao.impl.ReligionDaoImpl;
import model.Religion;
import service.ReligionService;

public class ReligionServiceImpl implements ReligionService{
	ReligionDao religionDao = new ReligionDaoImpl();

	@Override
	public Collection<Religion> listReligion() throws Exception {
		// TODO Auto-generated method stub
		return religionDao.listReligion();
	}

	@Override
	public void insertReligion(Religion religion) throws Exception {
		// TODO Auto-generated method stub
		religionDao.insertReligion(religion);
		
	}

	@Override
	public void updateReligion(Religion religion) throws Exception {
		// TODO Auto-generated method stub
		religionDao.updateReligion(religion);
		
	}

	@Override
	public void deleteReligion(int idReligion) throws Exception {
		// TODO Auto-generated method stub
		religionDao.deleteReligion(idReligion);
		
	}

}
