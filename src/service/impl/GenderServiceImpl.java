package service.impl;

import java.util.Collection;

import dao.GenderDao;
import dao.impl.GenderDaoImpl;
import model.Gender;
import service.GenderService;

public class GenderServiceImpl implements GenderService{
	GenderDao genderDao = new GenderDaoImpl();

	@Override
	public Collection<Gender> listGender() throws Exception {
		// TODO Auto-generated method stub
		return genderDao.listGender();
	}

	@Override
	public void insertGender(Gender gender) throws Exception {
		// TODO Auto-generated method stub
		genderDao.insertGender(gender);
	}

	@Override
	public void updateGender(Gender gender) throws Exception {
		// TODO Auto-generated method stub
		genderDao.updateGender(gender);
	}

	@Override
	public void deleteGender(int idGender) throws Exception {
		// TODO Auto-generated method stub
		genderDao.deleteGender(idGender);
		
	}

}
