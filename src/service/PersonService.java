package service;

import java.util.Collection;

import model.ListPerson;
import model.Person;

public interface PersonService {
	public Collection<ListPerson> listPerson () throws Exception;
	public Person getPerson (int idPerson) throws Exception;
	public void insertPerson (Person person) throws Exception;
	public void updatePerson (Person person) throws Exception;
	public void deletePerson (int idPerson) throws Exception;

}
