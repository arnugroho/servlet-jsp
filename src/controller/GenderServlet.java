package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Gender;
import service.GenderService;
import service.impl.GenderServiceImpl;

/**
 * Servlet implementation class GenderServlet
 */
@WebServlet("/GenderServlet")
public class GenderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	GenderService genderService = new GenderServiceImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GenderServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Collection<Gender> genderCollection = new ArrayList<Gender>();
		try {
			genderCollection = genderService.listGender();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.setAttribute("genderCollection", genderCollection);
		getServletContext().getRequestDispatcher("/WEB-INF/jsp/gender-list.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

}
