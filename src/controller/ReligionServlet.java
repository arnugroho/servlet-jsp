package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Religion;
import service.ReligionService;
import service.impl.ReligionServiceImpl;

/**
 * Servlet implementation class ReligionServlet
 */
@WebServlet("/ReligionServlet")
public class ReligionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ReligionService religionService = new ReligionServiceImpl();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ReligionServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setAttribute("action", "insert");
		if (request.getParameter("idReligion") != null) {
			request.setAttribute("idReligion", request.getParameter("idReligion"));
			request.setAttribute("religionName", request.getParameter("religionName"));
			request.setAttribute("action", request.getParameter("action"));
		} else {
			request.setAttribute("action", "insert");
		}
		listGender(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String action = request.getParameter("action");
		if (action.equalsIgnoreCase("insert")) {
			Religion religion = new Religion();
			religion.setIdReligion(Integer.parseInt(request.getParameter("idReligion")));
			religion.setReligionName(request.getParameter("religionName"));
			try {
				religionService.insertReligion(religion);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			request.setAttribute("action", "insert");
			listGender(request, response);
		} else if (action.equalsIgnoreCase("edit")) {
			Religion religion = new Religion();
			religion.setIdReligion(Integer.parseInt(request.getParameter("idReligion")));
			religion.setReligionName(request.getParameter("religionName"));
			try {
				religionService.updateReligion(religion);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			request.setAttribute("action", "insert");
			listGender(request, response);
		} else if (action.equalsIgnoreCase("delete")) {
			int idReligion = Integer.parseInt(request.getParameter("idReligion"));
			try {
				religionService.deleteReligion(idReligion);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			request.setAttribute("action", "insert");
			listGender(request, response);
		}

	}

	public void listGender(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Collection<Religion> religionCollection = new ArrayList<Religion>();
		try {
			religionCollection = religionService.listReligion();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.setAttribute("religionCollection", religionCollection);
		getServletContext().getRequestDispatcher("/WEB-INF/jsp/religion-list.jsp").forward(request, response);

	}

}
