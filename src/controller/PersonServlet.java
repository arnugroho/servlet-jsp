package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.ListPerson;
import model.Person;
import model.Religion;
import service.PersonService;
import service.ReligionService;
import service.impl.PersonServiceImpl;
import service.impl.ReligionServiceImpl;

/**
 * Servlet implementation class PersonServlet
 */
@WebServlet("/PersonServlet")
public class PersonServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	PersonService personService = new PersonServiceImpl();
	ReligionService religionService = new ReligionServiceImpl();
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PersonServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setAttribute("action", "insert");
		if (request.getParameter("idPerson") != null) {
			try {
				Person person = new Person();
				person = personService.getPerson(Integer.parseInt(request.getParameter("idPerson")));
				request.setAttribute("idPerson", Integer.parseInt(request.getParameter("idPerson")));
				request.setAttribute("firstName", person.getFirstName());
				request.setAttribute("lastName", person.getLastName());
				request.setAttribute("birthdate", person.getBirthdate());
				request.setAttribute("gender", person.getGender());
				request.setAttribute("religion", person.getReligion());
				request.setAttribute("address", person.getAddress());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			request.setAttribute("action", request.getParameter("action"));
		} else {
			request.setAttribute("action", "insert");
		}
		listPerson(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String action = request.getParameter("action");
		if (action.equalsIgnoreCase("insert")) {
			try {
				Person person = new Person();
				person.setFirstName(request.getParameter("firstName"));
				person.setLastName(request.getParameter("lastName"));
				person.setBirthdate(sdf.parse(request.getParameter("birthdate")));
				person.setGender(Integer.parseInt(request.getParameter("gender")));
				person.setReligion(Integer.parseInt(request.getParameter("religion")));
				person.setAddress(request.getParameter("address"));
				personService.insertPerson(person);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			request.setAttribute("action", "insert");
			listPerson(request, response);
		} else if (action.equalsIgnoreCase("delete")) {
			int idPerson = Integer.parseInt(request.getParameter("idPerson"));
			try {
				personService.deletePerson(idPerson);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			request.setAttribute("action", "insert");
			listPerson(request, response);
		} else if (action.equalsIgnoreCase("edit")) {
			try {
				Person person = new Person();
				person.setIdPerson(Integer.parseInt(request.getParameter("idPerson")));
				person.setFirstName(request.getParameter("firstName"));
				person.setLastName(request.getParameter("lastName"));
				person.setBirthdate(sdf.parse(request.getParameter("birthdate")));
				person.setGender(Integer.parseInt(request.getParameter("gender")));
				person.setReligion(Integer.parseInt(request.getParameter("religion")));
				person.setAddress(request.getParameter("address"));
				personService.updatePerson(person);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			request.setAttribute("action", "insert");
			listPerson(request, response);
		}
	}

	public void listPerson(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Collection<ListPerson> personCollection = new ArrayList<ListPerson>();
		Collection<Religion> religionCollection = new ArrayList<Religion>();
		try {
			personCollection = personService.listPerson();
			religionCollection = religionService.listReligion();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.setAttribute("personCollection", personCollection);
		request.setAttribute("religionCollection", religionCollection);
		getServletContext().getRequestDispatcher("/WEB-INF/jsp/person-list.jsp").forward(request, response);

	}

}
